## A propos de PDF EXTRACT

Avant de prendre le projet assurez-vous d'avoir installé GIT. Si c'est pas le cas vous le trouverez à l'adresse ci-après [Git](https://git-scm.com/download/win)
Assurez-vous aussi d'avoir installé un serveur telque wamp ou laragon pour pouvoir exécuter le projet. [Laragon](https://sourceforge.net/projects/laragon/files/releases/4.0/laragon-full.exe)
Installez aussi composer pour installer les différents paquetes demandés pour utiliser le projet. [Composer](https://getcomposer.org/download/)


Si vous avez déjà installé GIT dans votre machine, vous pouvez dès à présent clonner le projet en tapant sur la ligne de commandes (Assuerez vous d'être dans ma racine des projets de votre serveurs)

`git clone https://gitlab.com/ladinstar/pdf-extract.git`

Après avoir télécharger les fichiers du projet placez vous sur la racine du projet en tapant

`cd pdf-extract`

Vous pouvez dès à présent télécharger les dépendances en tapant cetter commande :

`composer install`

Cette commande va installer tout le nécesaire pour pouvoir utiliser le projet. 
Après avoir fait cela cliquez tapez cette commande :

`cp .env.example .env`

`php artisan key:generate`

A présent vous pouvez visiter le site en tapant http://localhost/pdf-extract/public
Ou si vous avez installé php dans votre PATH tapez la commande : 

`php artisan serve` 

Puis tapez **http://127.0.0.1:8000** dans votre navigateur