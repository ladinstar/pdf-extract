<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
    	return view('welcome');
    }

    public function check(Request $request){
    	
    	$request->validate([
            'file' => 'required|mimetypes:application/pdf|max:10000',
        ]);

        $file = uniqid('diplom_') . '.' . $request->file->extension();
        $request->file->move(public_path('uploads/diplom/'), $file);

    	$parser = new \Smalot\PdfParser\Parser();
		$pdf = $parser->parseFile(urldecode(public_path('uploads/diplom/'.$file)));
		// Retrieve all details from the pdf file.
		$text = $pdf->getText();

		$details  = $pdf->getDetails();
		$author_verification = true;
		if(array_key_exists("Author", $details)){
			if($details['Author'] != config("verifications.author")){
				$author_verification = false;
			}
		}
		else{
			$author_verification = false;
		}

		if(!$author_verification){
			$request->session()->flash('error', "Document non valide");
			return redirect()->route('home');
		}

		$pages  = $pdf->getPages();

		$pages_verification = true;
		if(count($pages) !== config("verifications.pages")){
			$pages_verification = false;
		}
		if(!$pages_verification){
			$request->session()->flash('error', "Document non valide");
			return redirect()->route('home');
		}


		$font_verification = true;
		foreach ($pages as $page) {
			$fonts = $page->getFonts();
			foreach ($fonts as $font) {
				$verification_font = str_replace(" ", "", trim(config("verifications.font")));
				if(strpos($font->getName(), $verification_font) === false){
					$font_verification = false;
					break;
				}
			}
		}

		if(!$font_verification){
			$request->session()->flash('error', "Document non valide");
			return redirect()->route('home');
		}
		
		
		$request->session()->flash('success', "Votre document est valide");
		return redirect()->route('home');

	}

	public function isStringInFile($file,$string){

        $handle = fopen($file, 'r');
        $valid = false; // init as false
        while (($buffer = fgets($handle)) !== false) {
            if (strpos($buffer, $string) !== false) {
                $valid = TRUE;
                break; // Once you find the string, you should break out the loop.
            }      
        }
        fclose($handle);

        return $valid;

    }
}
