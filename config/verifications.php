<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auhor name
    |--------------------------------------------------------------------------
    |
    | Ici on spécifie le nom de l'auteur du document à vérifier à chaque fois
    |
    */

    'author' => "Mougang",

    /*
    |--------------------------------------------------------------------------
    | Font name
    |--------------------------------------------------------------------------
    |
    | Ici on spécifie la police du document à vérifier
    | Cela signifie que le document sera invalide si les textes
    | ne respectent pas cette police
    |
    */
    'font' => 'Times New Roman',

    /*
    |--------------------------------------------------------------------------
    | Pages
    |--------------------------------------------------------------------------
    |
    | Nombre de page exact à vérifier pour le document
    */
   
   'pages' => 1,

];
