<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Vérifiez l'originalité de vos diplômes</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.10/dist/sweetalert2.min.css" rel="stylesheet"/>

        <!-- Styles -->
        <style>
        .frame {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 400px;
            height: 400px;
            margin-top: -200px;
            margin-left: -200px;
            border-radius: 2px;
            box-shadow: 1px 2px 10px 0px rgba(0, 0, 0, 0.3);
            background: #3A92AF;
            background: -webkit-linear-gradient(bottom left, #3A92AF 0%, #5CA05A 100%);
            background: -moz-linear-gradient(bottom left, #3A92AF 0%, #5CA05A 100%);
            background: -o-linear-gradient(bottom left, #3A92AF 0%, #5CA05A 100%);
            background: linear-gradient(to top right, #3A92AF 0%, #5CA05A 100%);
            color: #fff;
            font-family: 'Open Sans', Helvetica, sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .center {
            position: absolute;
            width: 300px;
            height: 260px;
            top: 70px;
            left: 50px;
            background: #fff;
            box-shadow: 8px 10px 15px 0 rgba(0, 0, 0, 0.2);
            border-radius: 3px;
        }
        .title {
            font-size: 16px;
            color: #676767;
            line-height: 50px;
            height: 50px;
            border-bottom: 1px solid #D8D8D8;
            text-align: center;
        }
        .dropzone {
            position: absolute;
            z-index: 1;
            box-sizing: border-box;
            display: table;
            table-layout: fixed;
            width: 100px;
            height: 80px;
            top: 86px;
            left: 100px;
            border: 1px dashed #A4A4A4;
            border-radius: 3px;
            text-align: center;
            overflow: hidden;
        }
        .dropzone.is-dragover {
            border-color: #666;
            background: #eee;
        }
        .dropzone .content {
            display: table-cell;
            vertical-align: middle;
        }
        .dropzone .upload {
            margin: 6px 0 0 2px;
        }
        .dropzone .filename {
            display: block;
            color: #676767;
            font-size: 14px;
            line-height: 18px;
        }
        .dropzone .input {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            opacity: 0;
        }
        .upload-btn {
            position: absolute;
            width: 140px;
            height: 40px;
            left: 80px;
            bottom: 24px;
            background: #6ECE3B;
            border-radius: 3px;
            text-align: center;
            line-height: 40px;
            font-size: 14px;
            box-shadow: 0 2px 0 0 #498C25;
            cursor: pointer;
            transition: all 0.2s ease-in-out;
            border: none;
            color: #fff;
        }
        .upload-btn:hover {
            box-shadow: 0 2px 0 0 #498C25, 0 2px 10px 0 #6ECE3B;
        }
        .bar {
            position: absolute;
            z-index: 1;
            width: 300px;
            height: 3px;
            top: 49px;
            left: 0;
            background: #6ECE3B;
            transition: all 3s ease-out;
            transform: scaleX(0);
            transform-origin: 0 0;
        }
        .bar.active {
            transform: scaleX(1) translate3d(0, 0, 0);
        }
        .syncing {
            position: absolute;
            top: 109px;
            left: 134px;
            opacity: 0;
        }
        .syncing.active {
            animation: syncing 3.2s ease-in-out;
        }
        .done {
            position: absolute;
            top: 112px;
            left: 132px;
            opacity: 0;
        }
        .done.active {
            animation: done 0.5s ease-in 3.2s;
            animation-fill-mode: both;
        }
        @keyframes syncing {
            0% {
                transform: rotate(0deg);
            }
            10% {
                opacity: 1;
            }
            90% {
                opacity: 1;
            }
            100% {
                transform: rotate(360deg);
                opacity: 0;
            }
        }
        @keyframes done {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }
        </style>
    </head>
    <body>
        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="frame">
              <div class="center">
                    <div class="bar"></div>
                    <div class="title">Glissez déposer le diplôme</div>
                    <div class="dropzone">
                        <div class="content">
                            <img src="https://100dayscss.com/codepen/upload.svg" class="upload">
                            <span class="filename"></span>
                            <input type="file" name="file" class="input" accept="pdf">
                        </div>
                    </div>
                    <img src="https://100dayscss.com/codepen/syncing.svg" class="syncing">
                    <img src="https://100dayscss.com/codepen/checkmark.svg" class="done">
                    <button type="submit" class="upload-btn">Envoyez le diplôme</button>
              </div>
            </div>
        </form>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.10/dist/sweetalert2.all.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script type="text/javascript">
            @foreach (['error', 'success'] as $key)
                @if(Session::has($key))
                    Swal.fire({
                        icon: '{{ $key }}',
                        title: '{{ ($key == "error") ? "Erreur !" : "Succès" }}',
                        text: "{{ Session::get($key) }}"
                    });
                @endif
            @endforeach
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: "{{ $error }}"
                    });
                @endforeach
            @endif
        </script>
        <script type="text/javascript">
            var droppedFiles = false;
            var fileName = '';
            var $dropzone = $('.dropzone');
            var $button = $('.upload-btn');
            var uploading = false;
            var $syncing = $('.syncing');
            var $done = $('.done');
            var $bar = $('.bar');
            var timeOut;

            $dropzone.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                e.preventDefault();
                e.stopPropagation();
            })
                .on('dragover dragenter', function() {
                $dropzone.addClass('is-dragover');
            })
                .on('dragleave dragend drop', function() {
                $dropzone.removeClass('is-dragover');
            })
                .on('drop', function(e) {
                droppedFiles = e.originalEvent.dataTransfer.files;
                fileName = droppedFiles[0]['name'];
                $('.filename').html(fileName);
                $('.dropzone .upload').hide();
            });

            $button.bind('click', function(e) {
                if($(this).html()!='Done'){
                    e.preventDefault();
                    startUpload();
                }
            });

            $("input:file").change(function (){
                fileName = $(this)[0].files[0].name;
                $('.filename').html(fileName);
                $('.dropzone .upload').hide();
            });

            function startUpload() {
                if (!uploading && fileName != '' ) {
                    uploading = true;
                    $button.html('Uploading...');
                    $dropzone.fadeOut();
                    $syncing.addClass('active');
                    $done.addClass('active');
                    $bar.addClass('active');
                    timeoutID = window.setTimeout(showDone, 3200);
                }
            }

            function showDone() {
                $button.html('Done');
            }
        </script>
    </body>
</html>
